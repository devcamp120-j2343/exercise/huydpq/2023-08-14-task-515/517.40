
import Animal from './components/animal.';

function App() {
  return (
    <div style={{margin: "50px"}}>
      <Animal kind = "Cat"/>
    </div>
  );
}

export default App;
