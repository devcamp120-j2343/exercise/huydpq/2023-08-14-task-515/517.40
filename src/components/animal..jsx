import { Component } from "react";
import img from "../assets/images/cat.jpg"

class Animal extends Component {

   render(){
    let {kind} = this.props;
    return (
        
        kind === "cat" ? <img height="100px" width="100px" src={img} alt="moew"/> : <p> meow not found</p>
    )
   }
}

export default Animal